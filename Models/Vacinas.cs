﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace proradis.Models
{
    public class Vacinas
    {
        [Key]
        public int Id{ get; set; }

        public string Fabricante { get; set; }
        public string Lote { get; set; }
        public DateTime DataValidade { get; set; }
        public int NumDoses { get; set; }
        public int IntervaloMinDoses { get; set; }

    }
}
