﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace proradis.Models
{
    public class Vacinacao
    {
        [Key]
        public int Id { get; set; }
        public int PacienteId { get; set; }
        public int VacinaId { get; set; }
        public int QtdDose { get; set; }
        public int Status { get; set; }
        public DateTime DataVacinacao { get; set; }
        public DateTime? ProximaDose { get; set; }

        //FKs
        [ForeignKey("PacienteId")]
        public virtual Pacientes Paciente { get; set; }

        [ForeignKey("VacinaId")]
        public virtual Vacinas Vacina { get; set; }
    }
}
