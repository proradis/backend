﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace proradis.Models
{
    public class Pacientes
    {
        [Key]
        public int Id { get; set; }

        public string Nome { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }

        public string Email { get; set; }

        [MaxLength(2)]
        public string Sexo { get; set; }

        [MaxLength(14)]
        public string Telefone { get; set; }
        
        [MaxLength(9)]
        public string CEP { get; set; }
        
        [MaxLength(2)]
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string? Complemento { get; set; }
    }
}
