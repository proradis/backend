﻿using Microsoft.EntityFrameworkCore;
using proradis.Models;

namespace proradis.Context
{
    public class PRContext : DbContext
    {
        public PRContext(DbContextOptions<PRContext> options) : base(options)
        { }

        public DbSet<Pacientes> Pacientes { get; set; }
        public DbSet<Vacinas> Vacinas { get; set; }
        public DbSet<Vacinacao> Vacinacao { get; set; }
    }
}
