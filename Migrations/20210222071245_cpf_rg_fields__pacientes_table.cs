﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace proradis.Migrations
{
    public partial class cpf_rg_fields__pacientes_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CPF",
                table: "Pacientes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RG",
                table: "Pacientes",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CPF",
                table: "Pacientes");

            migrationBuilder.DropColumn(
                name: "RG",
                table: "Pacientes");
        }
    }
}
