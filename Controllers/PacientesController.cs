﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using proradis.Context;
using proradis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace proradis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacientesController : ControllerBase
    {
        private readonly PRContext _dbContext;
        public PacientesController(PRContext context)
        {
            _dbContext = context;
        }

        [HttpGet]
        public async Task<ContentResult> GetAll()
        {
            ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                List<Pacientes> pacientes = await _dbContext.Pacientes.ToListAsync();

                result.Content = JsonConvert.SerializeObject(pacientes);
                result.StatusCode = (int)HttpStatusCode.OK;
            }catch(Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }

        [HttpPost]
        public async Task<ContentResult> Post([FromBody] Pacientes paciente)
        {
            ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                Pacientes verify = _dbContext.Pacientes
                    .FirstOrDefault(x => x.CPF == paciente.CPF || x.RG == paciente.RG);

                if(verify == null)
                {
                    _dbContext.Pacientes.Add(paciente);
                    await _dbContext.SaveChangesAsync();

                    result.Content = JsonConvert.SerializeObject(paciente);
                    result.StatusCode = (int)HttpStatusCode.Created;
                }
                else {
                    string message = "Este Paciente já existe";

                    result.Content = JsonConvert.SerializeObject(message);
                    result.StatusCode = (int)HttpStatusCode.OK;
                }
            } catch(Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }


            return result;
        }
    }
}
