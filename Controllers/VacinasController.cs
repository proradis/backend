using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using proradis.Context;
using proradis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace proradis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacinasController : ControllerBase
    {
        private readonly PRContext _dbContext;

        public VacinasController(PRContext context)
        {
            _dbContext = context;    
        }

        [HttpGet]
        public async Task<ContentResult> GetAll()
        {
            ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                List<Vacinas> vacinas = await _dbContext.Vacinas.ToListAsync();

                result.Content = JsonConvert.SerializeObject(vacinas);
                result.StatusCode = (int)HttpStatusCode.OK;
            }catch(Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }

        [HttpPost]
        public async Task<ContentResult> Post([FromBody] Vacinas vacina)
        {
             ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                Vacinas verify = _dbContext.Vacinas
                    .FirstOrDefault(x => x.Fabricante == vacina.Fabricante);

                if (verify == null)
                {
                    _dbContext.Vacinas.Add(vacina);
                    await _dbContext.SaveChangesAsync();

                    result.Content = JsonConvert.SerializeObject(vacina);
                    result.StatusCode = (int)HttpStatusCode.Created;
                }
                else
                {
                    string message = "Esta Vacina já existe";

                    result.Content = JsonConvert.SerializeObject(message);
                    result.StatusCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }


            return result;
        }
    }
}