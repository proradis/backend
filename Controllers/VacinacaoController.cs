﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using proradis.Context;
using proradis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace proradis.Controllers
{
    [Route("api/Registros")]
    [ApiController]
    public class VacinacaoController : ControllerBase
    {
        private readonly PRContext _dbContext;

        public VacinacaoController(PRContext context)
        {
            _dbContext = context;
        }

        [HttpGet]
        [Route("Pacientes/{pacienteId}")]
        public async Task<ContentResult> getPacientes(int pacienteId)
        {
            ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                Vacinacao infos = await _dbContext.Vacinacao
                    .OrderByDescending(x => x.Id)
                    .FirstOrDefaultAsync(x => x.PacienteId == pacienteId);

                bool erro = false;
                string mensagem = String.Empty;

                if (infos != null)
                {

                    //verifica se o Paciente já terminou a vacinação
                    if (infos.Status == 1)
                    {
                        erro = true;
                        mensagem = "Paciente já tomou todas as doses de vacinação";
                    }
                    //verifica o tempo entre hoje e a ultima vacinacao
                    else if (infos.ProximaDose > DateTime.Now)
                    {
                        int diasRestante = ((DateTime)infos.ProximaDose - DateTime.Now).Days;

                        erro = true;
                        mensagem = "Intervalo entre doses insuficiente. Aguarde mais " + diasRestante.ToString() + " dias";
                    }
                }

                var response = new
                {
                    Erro = erro,
                    Mensagem = mensagem,
                    Objeto = infos
                };

                result.Content = JsonConvert.SerializeObject(response);
                result.StatusCode = (int)HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }

        [HttpGet]
        public async Task<ContentResult> GetAll()
        {
            ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                var registros = await _dbContext.Vacinacao
                    .Select(a => new
                    {
                        a.Id,
                        a.DataVacinacao,
                        Paciente = a.Paciente.Nome + " - " +a.Paciente.CPF + " - " +a.Paciente.RG,
                        Vacina = a.Vacina.Fabricante
                    })
                    .ToListAsync();

                result.Content = JsonConvert.SerializeObject(registros);
                result.StatusCode = (int)HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }

        [HttpPost]
        public async Task<ContentResult> Post([FromBody] Vacinacao vacinacao)
        {
            ContentResult result = new ContentResult();
            result.ContentType = "application/json";

            try
            {
                bool erro = false;
                string mensagem = String.Empty;

                Vacinacao novaVacinacao = new Vacinacao();
                if (vacinacao.DataVacinacao <= DateTime.Now)
                {

                    var verify = await _dbContext.Vacinacao
                    .OrderByDescending(x => x.Id)
                    .Select(a => new
                    {
                        a.Id,
                        a.PacienteId,
                        a.VacinaId,
                        a.Vacina.NumDoses,
                        a.QtdDose
                    })
                    .FirstOrDefaultAsync(x => x.PacienteId == vacinacao.PacienteId);

                    novaVacinacao.PacienteId = vacinacao.PacienteId;
                    novaVacinacao.VacinaId = vacinacao.VacinaId;
                    novaVacinacao.DataVacinacao = vacinacao.DataVacinacao;

                    if (verify != null)
                    {

                        if (verify.VacinaId != vacinacao.VacinaId)
                        {
                            erro = true;
                            mensagem = "O Paciente não pode ser vacinado com essa Vacina";
                        }

                        if (verify.QtdDose + 1 == verify.NumDoses)
                        {
                            novaVacinacao.Status = 1;
                            novaVacinacao.ProximaDose = null;
                            novaVacinacao.QtdDose = verify.QtdDose + 1;
                        }
                    }
                    else
                    {
                        Vacinas vacina = _dbContext.Vacinas.FirstOrDefault(x => x.Id == novaVacinacao.VacinaId);

                        DateTime proximaDose = vacinacao.DataVacinacao.AddDays(vacina.IntervaloMinDoses);
                        novaVacinacao.Status = 0;
                        novaVacinacao.QtdDose = 1;
                        novaVacinacao.ProximaDose = proximaDose;
                    }
                }
                else
                {
                    erro = true;
                    mensagem = "Data de vacinação é maior que o permitido";
                }

                if (!erro)
                {
                    _dbContext.Vacinacao.Add(novaVacinacao);
                    await _dbContext.SaveChangesAsync();

                    mensagem = "Registro criado com sucesso!";

                    result.StatusCode = (int)HttpStatusCode.Created;
                }
                else
                {
                    result.StatusCode = (int)HttpStatusCode.OK;
                }

                var response = new
                {
                    Erro = erro,
                    Mensagem = mensagem,
                    Objeto = novaVacinacao
                };
                result.Content = JsonConvert.SerializeObject(response);
            }
            catch (Exception ex)
            {
                result.Content = JsonConvert.SerializeObject(ex.Message);
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
            }


            return result;
        }
    }
}